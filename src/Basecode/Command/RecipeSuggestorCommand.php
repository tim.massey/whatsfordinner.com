<?php

namespace Basecode\Command;

use Basecode\RecipeSuggestor;
use Basecode\Loader\IngredientLoader;
use Basecode\Loader\RecipeLoader;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RecipeSuggestorCommand extends Command
{
	protected function configure()
	{
		$this
		->setName('suggest')
		->setDescription('Make a suggestion')
		->addOption(
        	'ingredients',
        	'i',
        	InputOption::VALUE_OPTIONAL,
        	'Path to the list of ingredients (CSV)',
        	'ingredients.csv'
    	)
    	->addOption(
    			'recipes',
    			'r',
    			InputOption::VALUE_OPTIONAL,
    			'Path to the list of recipes (JSON)',
    			'recipes.json'
    	);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$ingredientsFile = $input->getOption('ingredients');

		if (!$this->validIngredientsFile($ingredientsFile)) {
			$output->writeln('Invalid ingredients file');
			return false;
		}

		$recipiesFile = $input->getOption('recipes');
		if (!$this->validRecipesFile($recipiesFile)) {
			$output->writeln('Invalid recipes file');
			return false;
		}

		$suggestor = $this->getSuggestor($ingredientsFile, $recipiesFile);
		$suggestion = $suggestor->makeSuggestion();

		$output->writeln($suggestion);
	}

	protected function validIngredientsFile($path)
	{
		$pathInfo = $this->getFileInfo($path);
		if ($pathInfo) {
			return $pathInfo['extension'] == 'csv';
		}
		return false;
	}

	protected function validRecipesFile($path)
	{
		$pathInfo = $this->getFileInfo($path);
		if ($pathInfo) {
			return $pathInfo['extension'] == 'json';
		}
		return false;
	}

	protected function getFileInfo($path)
	{
		if (!file_exists($path)) {
			return false;
		}
		return pathinfo($path);
	}

	protected function getSuggestor($ingredientsFile, $recipesFile)
	{
		$ingredientLoader = new IngredientLoader($ingredientsFile);
		$recipeLoader = new RecipeLoader($recipesFile);
		return new RecipeSuggestor($ingredientLoader, $recipeLoader);
	}
}

<?php
namespace Basecode;

use Basecode\Loader\IngredientLoader;
use Basecode\Loader\RecipeLoader;
use Basecode\Recipe;
use Basecode\Ingredient;

class RecipeSuggestor
{

    protected $ingredientLoader;

    protected $recipeLoader;

    static $defaultSuggestion = 'Order Takeout';

    public function __construct(IngredientLoader $ingredientLoader, RecipeLoader $recipeLoader)
    {
        $this->ingredientLoader = $ingredientLoader;
        $this->recipeLoader = $recipeLoader;
    }

    /**
     * Make a suggestion based on the available ingredients and recipe "database".
     *
     * @return string
     */
    public function makeSuggestion()
    {

        /*
         * Build up an array of fresh ingredients to choose from.
         */
        $fridgeContents = $this->ingredientLoader->load();
        $freshIngredients = array();
        foreach ($fridgeContents as $ingredient) {
            if (! $ingredient->isStale()) {
                @$freshIngredients[$ingredient->getName()][$ingredient->getUnit()] += $ingredient->getAmount();
                $expiryList[$ingredient->getUseBy()->getTimestamp()][] = $ingredient;
            }
        }

        /*
         * Loop over the potential recipes and check if you have enough fresh ingredients.
         */
        $recipes = $this->recipeLoader->load();
        $potentialRecipes = array();
        foreach ($recipes as $recipe) {
            if ($this->canMake($recipe, $freshIngredients)) {
                $potentialRecipes[] = $recipe;
            }
        }

        if (empty($potentialRecipes))
            return self::$defaultSuggestion;

        if (count($potentialRecipes) == 1) {
            return $potentialRecipes[0]->getName();
        }

        $selectedRecipe = $this->chooseByUseByDate($recipes, $expiryList);
        return $selectedRecipe->getName();
    }

    /**
     * Determine if a recipe can be made based on the available ingredients.
     *
     * @param Recipe $recipe
     * @param array $freshIngredients
     * @return boolean
     */
    protected function canMake(Recipe $recipe, $freshIngredients)
    {
        foreach ($recipe->getIngredients() as $ingredient) {

            /*
             * We dont have any fresh.
             */
            if (! isset($freshIngredients[$ingredient->getName()])) {
                return false;
            }

            /*
             * We dont have enough.
             */
            if ($ingredient->getAmount() >= @$freshIngredients[$ingredient->getName()][$ingredient->getUnit()]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Relatively naieve function to choose which recipe should be suggested
     * based on the oldest ingredient.
     *
     * Further "smarts" could be added based on the quantity required &/or age of the
     * other components of the recipe.
     *
     * @param
     *            array[] Recipe $recipes
     * @param
     *            array[] Ingredient $expiryList
     * @return Recipe boolean
     */
    protected function chooseByUseByDate($recipes, $expiryList)
    {
        /*
         * Make sure the expiry list is sorted (closest to expiry at the top)
         */
        ksort($expiryList);

        $oldestIngredient = array_shift($expiryList);

        /*
         * Loop over the old ingredients and return the first recipe that contains any of it.
         */
        foreach ($oldestIngredient as $ingredient) {
            foreach ($recipes as $r) {
                if ($r->contains($ingredient)) {
                    return $r;
                }
            }
        }

        return false;
    }
}
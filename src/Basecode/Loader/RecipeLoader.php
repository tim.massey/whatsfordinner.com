<?php

namespace Basecode\Loader;

use Basecode\Recipe;
use Basecode\Ingredient;

class RecipeLoader
{

	protected $path = null;

	protected $recipes = array();

	protected $errors = array();

	/**
	 * Construct with a full filepath to the recipe data file.
	 * @param string $path
	 * @throws \Exception
	 */
	public function __construct($path)
	{
		if(!file_exists($path)) {
			throw new \Exception('File does not exist: '.$path);
		}
		$this->path = $path;
	}

	/**
	 * Load the recipes from disk and return as array of Recipe objects.
	 * Will assign property of error strings if there are problems constructing the recipe/ingredients.
	 * @return array
	 */
	public function load()
	{

		$rawRecipes = $this->loadFromDisk();

		try{

			foreach($rawRecipes as $r) {
				$recipe = new Recipe($r->name);
				if(is_array($r->ingredients)) foreach($r->ingredients as $i) {
					$ingredient = new Ingredient($i->item, $i->amount, $i->unit);
					$recipe->setIngredient($ingredient);
				}
				$this->setRecipe($recipe);
			}

		} catch(\Exception $e) {
			$this->errors[] = $e->getMessage();
		}

		return $this->recipes;
	}

	/**
	 * Load the raw recipe data from disk.
	 * @return mixed
	 */
	protected function loadFromDisk()
	{
		return json_decode(file_get_contents($this->path));
	}

	/**
	 * Assign a loaded recipe.
	 * @param Recipe $recipe
	 */
	protected function setRecipe(Recipe $recipe)
	{
		$this->recipes[] = $recipe;
	}

}
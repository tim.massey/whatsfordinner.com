<?php

namespace Basecode\Loader;

use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;
use Basecode\Ingredient;

class IngredientLoader
{

	protected $path = null;

	protected $ingredients;

	/**
	 *
	 * @param string $path
	 * @throws \Exception
	 */
	public function __construct($path)
	{
		if(!file_exists($path)) {
			throw new \Exception('File does not exist: '.$path);
		}
		$this->path = $path;
	}

	/**
	 * Parse the ingredient list and construct ingredient objects
	 * @return array of Ingredient objects
	 */
	public function load()
	{
		$lexer = new Lexer(new LexerConfig());
		$interpreter = new Interpreter();
		$interpreter->addObserver(function(array $row) {
			$ingredient = new Ingredient($row[0], $row[1], $row[2]);
			$ingredient->setUseBy($row[3]);
			$this->setIngredient($ingredient);
		});
		$lexer->parse($this->path, $interpreter);
		return $this->ingredients;
	}

	/**
	 * Assign an ingredient to the list
	 * @param Ingredient $ingredient
	 */
	public function setIngredient(Ingredient $ingredient)
	{
		$this->ingredients[] = $ingredient;
	}

}
<?php
namespace Basecode;

class Ingredient
{
	protected $name;

	protected $unit;

	protected $useBy;

	public function __construct($name, $amount, $unit, $useBy = null)
	{
		$this->name = trim($name);
		$this->amount = (int)$amount;
		$this->setUnit(trim($unit));
		if(!is_null($useBy)) {
			$this->setUseBy($useBy);
		}
	}

	public function setUseBy($useBy)
	{
		$this->useBy = \DateTime::createFromFormat('d/m/Y', trim($useBy));
	}

	public function getUseBy()
	{
		return $this->useBy;
	}

	public function isStale()
	{
		$now = new \DateTime();
		return $now >= $this->useBy;
	}

	public function setUnit($unit)
	{
		if (!$this->validUnit($unit)) {
			throw new \Exception('Invalid unit: '.$unit);
		}

		$this->unit = $unit;

	}

	public function validUnit($unit)
	{
		return in_array($unit, array(
			'ml',
			'slices',
			'grams',
			'of',
		));
	}

	public function getName()
	{
		return $this->name;
	}

	public function getUnit()
	{
		return $this->unit;
	}

	public function getAmount()
	{
		return $this->amount;
	}

}
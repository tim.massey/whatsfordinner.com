# Code sample

Sample code to select recipes based on:

Given a list of items in the fridge (presented as a csv list), and a collection of recipes (a
collection of JSON formatted recipes), produce a recommendation for what to cook tonight.
Program should be written to take two inputs; fridge csv list, and the json recipe data. How you
choose to implement this is up to you; you can write a console application which takes input file
names as command line args, or as a web page which takes input through a form.

The only rule is that it must run and return a valid result using the provided input data.

## Inputs

```CSV
bread,10,slices,25/12/2014
cheese,10,slices,25/12/2014
butter,250,grams,25/12/2014
peanut butter,250,grams,2/12/2014
mixed salad,500,grams,26/12/2013
```

```json
[
{
"name": "grilled cheese on toast",
"ingredients": [
{ "item":"bread", "amount":"2", "unit":"slices"},
{ "item":"cheese", "amount":"2", "unit":"slices"}
]
}
,
{
"name": "salad sandwich",
"ingredients": [
{ "item":"bread", "amount":"2", "unit":"slices"},
{ "item":"mixed salad", "amount":"200", "unit":"grams"}
]
}
]
```

## Installation

`composer install`

## Demo

`./suggestor.php suggest -i tests/Unit/Fixtures/ingredients.csv -r tests/Unit/Fixtures/recipes.json`

## Tests

`vendor/bin/phpunit -c tests/phpunit.xml tests`
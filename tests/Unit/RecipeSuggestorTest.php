<?php
namespace Tests\Unit;

use Basecode\Loader\IngredientLoader;
use Basecode\Loader\RecipeLoader;
use Basecode\RecipeSuggestor;
use Basecode\Ingredient;
use Basecode\Recipe;

class RecipeSuggestorCommandTest extends \PHPUnit_Framework_TestCase
{
	public function testItWorks()
	{

		$ingredientLoader = new IngredientLoader(__DIR__.'/Fixtures/ingredients.csv');
		$recipeLoader = new RecipeLoader(__DIR__.'/Fixtures/recipes.json');

		$suggestor = new RecipeSuggestor($ingredientLoader, $recipeLoader);
		$actual = $suggestor->makeSuggestion();
		$this->assertEquals('grilled cheese on toast', $actual);

	}

	public function testReturnsDefaultSuggestionIfNoIngredients()
	{
		$ingredientLoader = $this
			->getMockBuilder('Basecode\Loader\IngredientLoader')
			->disableOriginalConstructor()
			->setMethods(array('load'))
			->getMock();

		$ingredientLoader
			->expects($this->once())
			->method('load')
			->will($this->returnValue(array()));

		$recipeLoader = new RecipeLoader(__DIR__.'/Fixtures/recipes.json');

		$suggestor = new RecipeSuggestor($ingredientLoader, $recipeLoader);
		$actual = $suggestor->makeSuggestion();

		$this->assertEquals(RecipeSuggestor::$defaultSuggestion, $actual);
	}

	public function testReturnsDefaultSuggestionIfNoRecipes()
	{

		$ingredientLoader = new IngredientLoader(__DIR__.'/Fixtures/ingredients.csv');

		$recipeLoader = $this
			->getMockBuilder('Basecode\Loader\RecipeLoader')
			->disableOriginalConstructor()
			->setMethods(array('load'))
			->getMock();

		$recipeLoader
			->expects($this->once())
			->method('load')
			->will($this->returnValue(array()));


		$suggestor = new RecipeSuggestor($ingredientLoader, $recipeLoader);
		$actual = $suggestor->makeSuggestion();

		$this->assertEquals(RecipeSuggestor::$defaultSuggestion, $actual);
	}

	public function testChoosesTheCorrectRecipeIfMultipleOptions()
	{

        $fridge = array(
            new Ingredient('Bread', 5, 'slices', date('d/m/Y', strtotime('+1 week'))),
            new Ingredient('Tomato', 5, 'of', date('d/m/Y', strtotime('+2 days'))),
            new Ingredient('Flour', 500, 'grams', date('d/m/Y', strtotime('tomorrow'))),
            new Ingredient('Cheese', 250, 'grams', date('d/m/Y', strtotime('+1 week'))),
            new Ingredient('Egg', 12, 'of', date('d/m/Y', strtotime('tomorrow'))),
	    );

	    $ingredientLoader = $this
            ->getMockBuilder('Basecode\Loader\IngredientLoader')
            ->disableOriginalConstructor()
            ->setMethods(array('load'))
            ->getMock();

	    $ingredientLoader
	       ->expects($this->any())
	       ->method('load')
	       ->will($this->returnValue($fridge));

	    $eggsOnToast = new Recipe('Eggs on toast');
	    $eggsOnToast->setIngredient(new Ingredient('Egg', 2, 'of'));
	    $eggsOnToast->setIngredient(new Ingredient('Bread', 2, 'slices'));

	    $cheeseSambo = new Recipe('Cheese sandwich');
	    $cheeseSambo->setIngredient(new Ingredient('Cheese', 50, 'grams'));
	    $cheeseSambo->setIngredient(new Ingredient('Bread', 2, 'slices'));
	    $cheeseSambo->setIngredient(new Ingredient('Tomato', 1, 'of'));

	    $recipeLoader = $this
            ->getMockBuilder('Basecode\Loader\RecipeLoader')
            ->disableOriginalConstructor()
            ->setMethods(array('load'))
            ->getMock();

	    $recipeLoader
	       ->expects($this->any())
	       ->method('load')
	       ->will($this->returnValue(array($eggsOnToast, $cheeseSambo)));

	    $suggestor = new RecipeSuggestor($ingredientLoader, $recipeLoader);
	    $actual = $suggestor->makeSuggestion();

	    $this->assertEquals($actual, 'Eggs on toast');

	}

}
<?php
namespace Tests\Unit\Command;

use Basecode\Command\RecipeSuggestorCommand;

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class RecipeSuggestorCommandTest extends \PHPUnit_Framework_TestCase
{
	public function testInvalidIngredientsFileFails()
	{
		$command = $this->getMock('Basecode\Command\RecipeSuggestorCommand', array('validIngredientsFile'));
		$command
			->expects($this->once())
			->method('validIngredientsFile')
			->will($this->returnValue(false));

		$application = new Application();
		$application->add($command);

		$command = $application->find('suggest');
		$commandTester = new CommandTester($command);
		$commandTester->execute(array(
			'command' => $command->getName(),
			'--ingredients' => 'ingredients.csv',
			'--recipes' => 'recipes.json',
		));

		$this->assertStringStartsWith('Invalid', $commandTester->getDisplay());

	}

	public function testInvalidRecipeFileFails()
	{
		$command = $this->getMock('Basecode\Command\RecipeSuggestorCommand', array('validIngredientsFile', 'validRecipesFile'));

		$command
			->expects($this->once())
			->method('validIngredientsFile')
			->will($this->returnValue(true));

		$command
			->expects($this->once())
			->method('validRecipesFile')
			->will($this->returnValue(false));

		$application = new Application();
		$application->add($command);

		$command = $application->find('suggest');
		$commandTester = new CommandTester($command);
		$commandTester->execute(array(
			'command' => $command->getName(),
			'--ingredients' => 'ingredients.csv',
			'--recipes' => 'recipes.json',
		));

		$this->assertStringStartsWith('Invalid', $commandTester->getDisplay());

	}

	public function testSuggestorInstantiatedCorrectlyIfFilesAllGood()
	{
		$command = $this->getMock('Basecode\Command\RecipeSuggestorCommand', array('validIngredientsFile', 'validRecipesFile', 'getSuggestor'));

		$command
			->expects($this->once())
			->method('validIngredientsFile')
			->will($this->returnValue(true));

		$command
			->expects($this->once())
			->method('validRecipesFile')
			->will($this->returnValue(true));

		$suggestor = $this
			->getMockBuilder('Basecode\RecipeSuggestor')
			->disableOriginalConstructor()
			->setMethods(array('makeSuggestion'))
			->getMock();

		$suggestor
			->expects($this->once())
			->method('makeSuggestion')
			->will($this->returnValue('salad sandwich'));

		$command
			->expects($this->once())
			->method('getSuggestor')
			->with('ingredients.csv', 'recipes.json')
			->will($this->returnValue($suggestor));

		$application = new Application();
		$application->add($command);

		$command = $application->find('suggest');
		$commandTester = new CommandTester($command);
		$commandTester->execute(array(
			'command' => $command->getName(),
			'--ingredients' => 'ingredients.csv',
			'--recipes' => 'recipes.json',
		));

		$this->assertStringStartsWith('salad', $commandTester->getDisplay());

	}

}
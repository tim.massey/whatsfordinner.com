#!/usr/bin/env php
<?php

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Basecode\Command\RecipeSuggestorCommand;

$application = new Application("Recipe Suggestor", "1.0");
$application->add(new RecipeSuggestorCommand());
$application->run();